# Portail des Bricodages - lmdbt.fr

Bienvenue sur le Portail des Bricodages ! Ce site web regroupe plusieurs applications web et contenus pédagogiques libres, créés et partagés sous licence CC-BY pour les enseignants et créateurs de contenu éducatif.

## Applications Disponibles

1. **[CréaCarte](https://lmdbt.forge.apps.education.fr/creacarte/)** :
   - Conception et édition de cartes interactives personnalisées.
   - Idéale pour créer des cartes imprimables pour l'enseignement.

2. **[La TeXiothèque](https://lmdbt.forge.apps.education.fr/latexiotheque/)** :
   - Espace dédié aux ressources pédagogiques en LaTeX.
   - Partage de documents pédagogiques en PDF.

3. **[Pixel-It](https://lmdbt.forge.apps.education.fr/pixel-it/)** :
   - Transformation d'images en pixel art.
   - Création de grilles de pixel art à colorier.

4. **[Qui Corrige ?!](https://lmdbt.forge.apps.education.fr/quicorrige/)** :
   - Application de gestion de classe pour déterminer dynamiquement l'élève interrogé.
   - Tirage au sort et sauvegarde des listes d'élèves.

5. **[md2html](https://lmdbt.forge.apps.education.fr/md2html/)** :
   - Convertisseur de Markdown en HTML.
   - Supporte CSS intégré, équations MathJax, FontAwesome.

6. **[Gbd1m2p](https://lmdbt.forge.apps.education.fr/gbd1m2p/)** :
   - Générateur de mots de passe sécurisés et faciles à retenir à partir d'une phrase.

7. **[Tables × Nom](https://lmdbt.forge.apps.education.fr/tablesxnom/)** :
   - Générateur de tables de multiplication nominatives en LaTeX.
   - Guide pas à pas même pour les débutants.

8. **[md2html_code](https://lmdbt.forge.apps.education.fr/md2html_code/)** :
   - Application pour convertir du code Markdown en code HTML.
   - Utile quand le Markdown n'est pas pris en charge.

## Licence

Ce projet est sous licence [CC-BY](https://creativecommons.org/licenses/by/4.0/). Vous êtes libre de partager et d'adapter le contenu, à condition d'attribuer l'œuvre de la manière indiquée par l'auteur.

---

Pour toute question ou suggestion, n'hésitez pas à me contacter.
